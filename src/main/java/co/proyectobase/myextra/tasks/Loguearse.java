package co.proyectobase.myextra.tasks;

import static co.proyectobase.myextra.userinterface.LoginPage.USUARIO;
import static co.proyectobase.myextra.userinterface.LoginPage.PASSWORD;

import java.util.List;

import co.proyectobase.myextra.interactions.Digitar;
import co.proyectobase.myextra.interactions.Escribir;
import co.proyectobase.myextra.interactions.Ir;
import co.proyectobase.myextra.model.Tecla;
import co.proyectobase.myextra.userinterface.LoginPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;

public class Loguearse implements Task {
//	private List<Credenciales> datos;
//	
//	public Loguearse(List<Credenciales> datos) {
//	       this.datos = datos;
//}

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Escribir.elTexto("cjhsoto").en(USUARIO));
		actor.attemptsTo(Escribir.elTexto("jorge289").en( LoginPage.PASSWORD));
		actor.attemptsTo(Digitar.laTecla(Tecla.ENTER.getTecla()));
		actor.attemptsTo(Digitar.laTecla(Tecla.ENTER.getTecla()));
		actor.attemptsTo(Digitar.laTecla(Tecla.ENTER.getTecla()));
		actor.attemptsTo(Digitar.laTecla(Tecla.ENTER.getTecla()));
		//actor.attemptsTo(Ir.hastaVerTexto(datos.get(0).getTexo()).en(UBICACION));
		
		
	}

	public static Loguearse myExtra() {
		
		return Tasks.instrumented(Loguearse.class);
	}
	

}
