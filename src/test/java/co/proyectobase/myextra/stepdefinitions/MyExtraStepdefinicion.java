package co.proyectobase.myextra.stepdefinitions;

import org.openqa.selenium.WebDriver;

import co.proyectobase.myextra.tasks.Abrir;
import co.proyectobase.myextra.tasks.Loguearse;
import co.proyectobase.myextra.tasks.Navegar;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.sourceforge.htmlunit.corejs.javascript.NativeGenerator;
import net.thucydides.core.annotations.Managed;

public class MyExtraStepdefinicion {
	@Managed(driver="chrome")
	private WebDriver hisBrowser;
	private Actor jorge = Actor.named("Jorge");
	
	@Before
	public void configuracionInicial() {
		jorge.can(BrowseTheWeb.with(hisBrowser));
		}
	
	@Dado("^que Jorge desea realizar consultas en As, Jorge abre my extra$")
	public void queJorgeDeseaRealizarConsultasEnAsJorgeAbreMyExtra(){
		jorge.attemptsTo(Abrir.myExtra());
		
	  
	}


	@Cuando("^él ingresa sus credenciales$")
	public void élIngresaSusCredenciales()  {
		jorge.attemptsTo(Loguearse.myExtra());
	   
	}

	@Cuando("^Navega en un menu$")
	public void navegaEnUnMenu() {
		jorge.attemptsTo(Navegar.aUnMenu());
	    
	}

	@Entonces("^verifica que inició sesión corectamente$")
	public void verificaQueInicióSesiónCorectamente() throws Exception {
	   
	}

}
